<?php
	if(!@defined('LIBRARIES')) die("Error");

	@define(UPLOAD_FILE,'../upload/download/');
	@define(UPLOAD_FILE_L,'upload/download/');

	@define(UPLOAD_SYNC,'../upload/sync/');
	@define(UPLOAD_SYNC_L,'upload/sync/');

	@define(UPLOAD_EXCEL,'../upload/excel/');
	@define(UPLOAD_EXCEL_L,'upload/excel/');

	@define(UPLOAD_SEOPAGE,'../upload/seopage/');
	@define(UPLOAD_SEOPAGE_L,'upload/seopage/');
	
	@define(UPLOAD_PHOTO,'../upload/photo/');
	@define(UPLOAD_PHOTO_L,'upload/photo/');

	@define(UPLOAD_TEMP,'../upload/temp/');
	@define(UPLOAD_TEMP_L,'upload/temp/');

	@define(UPLOAD_USER,'../upload/user/');
	
	@define(UPLOAD_PRODUCT,'../upload/product/');
	@define(UPLOAD_PRODUCT_L,'upload/product/');

	@define(UPLOAD_COLOR,'../upload/mau/');
	@define(UPLOAD_COLOR_L,'upload/mau/');
	
	@define(UPLOAD_NEWS,'../upload/news/');
	@define(UPLOAD_NEWS_L,'upload/news/');

	@define(UPLOAD_TAGS,'../upload/tags/');
	@define(UPLOAD_TAGS_L,'upload/tags/');
?>